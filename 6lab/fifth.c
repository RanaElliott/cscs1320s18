#include <stdio.h>

int length( char * string );
//length of the string
char charAt( char * string, int position );
//character at a given number in the string
int findChar(char * string, char find );
//position of the first occurance of something in a string
int findNextChar( char * string, char find, int start );

int findString( char * string, char * find );

int findNextString( char * string, char * find, int start );

int replaceChar( char * string, char target, char sub, int count);

char * split(char * string, int position );

char ** parse( char * string, char delimiter );

int a = 0;

int main(int argc, char *argv[ ] )
{
	char * str = "hulahoops";
	printf("%d/n", str);
	printf("Enter what letter position you want\n");
	fscanf(stdin, "%d", &a);
	a = ( a - 1 );
	printf("The letter is %c\n", charAt( str , a ));

	return(0);
}

int length( char * string ) {
    int count = 0;
    while( *string != '\0' ) { count++; string++; }
    return count;
}

char charAt( char * string, int position ){

        return(string[position]);
}

int findChar(char * string, char find){
    int count = 0;
    while(string[count] != '\0'){
        if((string[count]) == find){
            return (count);
        }
        count ++;
    }
	 return -1;
}

int findNextChar( char * string, char find, int start ) {
    int count = start + 1;
    while ( string[ start ] != string [ count ] ) {
        count++;
    }
    if ( count > length(string) ){
        count = -1;
    }
    return count;
}
