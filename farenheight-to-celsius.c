#include <stdio.h>
	
	/* this is based off of a program that is in the textbook i am reading,
	I am still getting used to the syntax so it is almost identical*/

	int main()
	{
			  float far;
			  float celsius;
			  int lower, upper, step;

			  lower = 0;
			  upper = 300;
			  step = 20;

			  far = lower;
			  while (far <= upper) {
						 celsius = (5.0/9.0) * (far-32.0);
						 printf("%3.0f %6.1f \n", far, celsius);
						 far = far + step;
			  }
	}
