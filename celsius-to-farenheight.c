#include <stdio.h>
	
	/* this is based off of a program that is in the textbook i am reading,
	I am still getting used to the syntax so it is almost identical*/

	int main()
	{
			  float celsius, far;
			  int lower, upper, step;

			  lower = 0;
			  upper = 100;
			  step = 5;
			
			  printf("Celsius \t Farenheight\n");
			  celsius = lower;
			  while (celsius <= upper) {
						 far = (celsius) * (9.0/5.0) + 32.0;
						 printf("%3.0f \t \t %6.1f \n", celsius, far);
						 celsius = celsius + step;
			  }
			  return 0;
	}
