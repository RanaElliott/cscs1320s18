#include <stdio.h>
#include <stdlib.h>

struct item {
   char * value;
   struct item * next;
   struct item * previous;
};

int main( int argc, char *argv[ ] ) { 
   char * string;
   string = "rice";
   struct item a;
   a.value = string;
   a.next = NULL;
   a.previous = NULL;

   string = "spinach";
   struct item b;
   b.value = string;
   b.next = NULL;
   b.previous = &a;
   a.next = &b;

   string = "salmon";
   struct item c;
   c.value = string;
   c.next = NULL;
   c.previous = &b;
   b.next = &c;
	
	string= "chips";
	struct item d;
	d.value = string;
	d.next = NULL;
	d.previous = &c;
	c.next = &d;

	struct item * list;
	//making it list here because list->value does not exits and list->next is not null one early
	for ( list = &d ; list != NULL ; list = list->previous ){
		printf ( "%s\n", list->value );
	}

   return 0;
}
