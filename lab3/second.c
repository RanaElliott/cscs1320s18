#include<stdio.h>

int main()
{
	char * vd = "void";
	char * chr = "char";
	char * shrt = "short";
	char * integer = "int";
	char * lng = "long";
	char * lnglng = "long long";
	char * flt = "float";
	char * dbl = "double";
	char * sgnd = "signed";
	char * usgnd = "unsigned";
	char * bool = "_Bool";
	char * complex = "_Complex";

	printf("%-10s%d\n", vd, sizeof ( void ));
	printf("%-10s%d\n", chr, sizeof ( char ));
	printf("%-10s%d\n", shrt, sizeof ( short ));
	printf("%-10s%d\n", integer, sizeof ( int ));
	printf("%-10s%d\n", lng, sizeof ( long ));
	printf("%-10s%d\n", lnglng, sizeof ( long long ));
	printf("%-10s%d\n", flt, sizeof ( float ));
	printf("%-10s%d\n", dbl, sizeof ( double ));
	printf("%-10s%d\n", sgnd, sizeof ( signed ));	
	printf("%-10s%d\n", usgnd, sizeof ( unsigned ));
	printf("%-10s%d\n", bool, sizeof ( _Bool ));
	printf("%-10s%d\n", complex, sizeof ( _Complex ));


	return 0;
}
