#include<stdio.h>

int main()
{
	char *given[ ] = { "Raul", "Cynthia", "Angela", "Tim" };
	char *surname[ ] = { "Martinez", "Lin", "Stein", "Tanaka" };
	int id[ ] = { 101, 2234, 87, 4978 };
	float balance[ ] = { -24.73, 4756.34, 768.93, 2938.74 };
	
	for( int account = 0; account < 4; account++ )
	{fprintf( stdout, "%4u  %-9s %-9s %+8.2f\n", id[ account ], given[ account ], surname[ account ], balance[ account ] );
	}
	return 0;
}
