#include <stdio.h>

int main( int argc, char *argv[ ] ) { 
	int rows = 8;
	int triangle[ rows ][ rows ];
	triangle[ 0 ][ 0 ] = 1;
//	triangle[ 1 ][ 0 ] = 1;
//	triangle[ 1 ][ 1 ] = 1;
	for( int row = 2; row < rows; row++ ) {
	  triangle[ row ][ 0 ] = 1;
	  for( int col = 1; col < row - 1; col++ ) {
		triangle[ row ][ col ] = triangle[ row - 1 ][ col - 1 ] + triangle[ row - 1 ][ col ];
	  } 
	  triangle[ row ][ row - 1 ] = 1;
	}

	char *pad = " ";
	for( int row = 0; row < rows; row++ ) {
	  for( int col = 0; col < row; col++ ) {
		  printf( "%d", triangle[ row ][ col ] );
	  }
	  printf( "\n" );
	}

	return 0; 
}
