#include<stdio.h>

int main()
{
	char * vd = "void";
	char * chr = "char";
	char * shrt = "short";
	char * integer = "int";
	char * lng = "long";
	char * lnglng = "long long";
	char * flt = "float";
	char * dbl = "double";
	char * sgnd = "signed";
	char * usgnd = "unsigned";
	char * bool = "_Bool";
	char * complex = "_Complex";

	printf("%-10s%d\n", vd, sizeof ( vd ));
	printf("%-10s%d\n", chr, sizeof ( chr ));
	printf("%-10s%d\n", shrt, sizeof ( shrt ));
	printf("%-10s%d\n", integer, sizeof ( integer ));
	printf("%-10s%d\n", lng, sizeof ( lng ));
	printf("%-10s%d\n", lnglng, sizeof ( lnglng ));
	printf("%-10s%d\n", flt, sizeof ( flt ));
	printf("%-10s%d\n", dbl, sizeof ( dbl ));
	printf("%-10s%d\n", sgnd, sizeof ( sgnd ));	
	printf("%-10s%d\n", usgnd, sizeof ( usgnd ));
	printf("%-10s%d\n", bool, sizeof ( bool ));
	printf("%-10s%d\n", complex, sizeof ( complex ));


	return 0;
}
