#include <stdio.h>
#include <stdlib.h>
int main ()
{
int num[ 3 ][ 3 ][ 3 ];
num[ 0 ][ 0 ][ 0 ] = 1;
num[ 0 ][ 0 ][ 1 ] = 2;
num[ 0 ][ 0 ][ 2 ] = 3;
num[ 0 ][ 1 ][ 0 ] = 4;
num[ 0 ][ 1 ][ 1 ] = 5;
num[ 0 ][ 1 ][ 2 ] = 6;
num[ 0 ][ 2 ][ 0 ] = 7;
num[ 0 ][ 2 ][ 1 ] = 8;
num[ 0 ][ 2 ][ 2 ] = 9;
num[ 1 ][ 0 ][ 0 ] = 10;
num[ 1 ][ 0 ][ 1 ] = 20;
num[ 1 ][ 0 ][ 2 ] = 30;
num[ 1 ][ 1 ][ 0 ] = 40;
num[ 1 ][ 1 ][ 1 ] = 50;
num[ 1 ][ 1 ][ 2 ] = 60;
num[ 1 ][ 2 ][ 0 ] = 70;
num[ 1 ][ 2 ][ 1 ] = 80;
num[ 1 ][ 2 ][ 2 ] = 90;
num[ 2 ][ 0 ][ 0 ] = 100;
num[ 2 ][ 0 ][ 1 ] = 200;
num[ 2 ][ 0 ][ 2 ] = 300;
num[ 2 ][ 1 ][ 0 ] = 400;
num[ 2 ][ 1 ][ 1 ] = 500;
num[ 2 ][ 1 ][ 2 ] = 600;
num[ 2 ][ 2 ][ 0 ] = 700;
num[ 2 ][ 2 ][ 1 ] = 800;
num[ 2 ][ 2 ][ 2 ] = 900;
num[ 3 ][ 0 ][ 0 ] = 1000;
num[ 3 ][ 0 ][ 1 ] = 2000;
num[ 3 ][ 0 ][ 2 ] = 3000;
num[ 3 ][ 1 ][ 0 ] = 4000;
num[ 3 ][ 1 ][ 1 ] = 5000;
num[ 3 ][ 1 ][ 2 ] = 6000;
num[ 3 ][ 2 ][ 0 ] = 7000;
num[ 3 ][ 2 ][ 1 ] = 8000;
num[ 3 ][ 2 ][ 2 ] = 9000;

int *iter;
int *ones = (int *)malloc( 3 * sizeof (int) );
iter = ones;
*iter++ = 1;
*iter++ = 2;
*iter++ = 3;
iter = ones;
for( int index = 0; index < 3; index++ ) {
    printf( "ones: %p, %d\n", iter, *iter );
    iter++;
}

int *tens = (int *)malloc( 3 * sizeof (int) );
iter = tens;
*iter++ = 10;
*iter++ = 20;
*iter++ = 30;
iter = tens;
for( int index = 0; index < 3; index++ ) {
    printf( "tens: %p, %d\n", iter, *iter );
    iter++;
}

int *hundreds = (int *)malloc( 3 * sizeof (int) );
iter = hundreds;
*iter++ = 100;
*iter++ = 200;
*iter++ = 300;
iter = hundreds;
for( int index = 0; index < 3; index++ ) {
    printf( "hundreds: %p, %d\n", iter, *iter );
    iter++;
}

int *thousands = (int *)malloc( 3 * sizeof (int) );
iter = thousands;
*iter++ = 1000;
*iter++ = 2000;
*iter++ = 3000;
iter = thousands;
for( int index = 0; index < 3; index++ ) {
    printf( "thousands: %p, %d\n", iter, *iter );
    iter++;
}


return 0;
}
